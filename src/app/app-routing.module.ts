import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NormalTransactionsListComponent} from './screens/normal-transactions-list/normal-transactions-list.component';
import {EthValueOnAddressComponent} from './screens/eth-value-on-address/eth-value-on-address.component';
import {TokensAmountComponent} from './screens/tokens-amount/tokens-amount.component';


const routes: Routes = [
  {
    path: '',
    component: NormalTransactionsListComponent,
    pathMatch: 'full'
  },
  {
    path: 'eth',
    component: EthValueOnAddressComponent,
    pathMatch: 'full'
  },
  {
    path: 'tokens',
    component: TokensAmountComponent,
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
