(function(window) {
  window["env"] = window["env"] || {};

  // Environment variables
  window["env"]["railsHost"] = "${RAILS_HOST}";
  window["env"]["railsPort"] = "${RAILS_PORT}";
})(this);
