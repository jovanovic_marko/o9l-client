import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TransactionsService} from '../../shared/services/transactions/transactions.service';

@Component({
  selector: 'app-eth-value-on-address',
  templateUrl: './eth-value-on-address.component.html',
  styleUrls: ['./eth-value-on-address.component.scss']
})
export class EthValueOnAddressComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
    private transactionsService: TransactionsService
  ) { }

  ethAmountForm: FormGroup;
  transactions = null;
  loadingTransactions = false;
  loaded = false;
  ethAmount = 0;

  ngOnInit(): void {
    this.createEthValueForm();
  }

  public createEthValueForm() {
    this.ethAmountForm = this.formBuilder.group({
      address: ['', [Validators.required]],
      date: ['', [Validators.required]]
    });
  }

  public onEthAmountFormSubmit() {
    const data = {
      address: this.ethAmountForm.controls.address.value,
      start_block: 1
    };

    this.getNormalTransactions(data);
  }

  public getNormalTransactions(data) {
    this.setLoadingAndLoaded(true, false);
    this.transactionsService.getTransactions(data).subscribe(
      response => {
        console.log(response);
        this.transactions = response['result'];
        this.setLoadingAndLoaded(false, true);
        this.ethAmount = this.calculateETH(data.address);
      },
      error => {
        console.log(error);
        this.setLoadingAndLoaded(false, true);
      }
    );
  }

  public setLoadingAndLoaded(loading, loaded) {
    this.loaded = loaded;
    this.loadingTransactions = loading;
  }

  public calculateETH(address) {
    let amount = 0;
    this.transactions.forEach(transaction => {
      if (transaction.from == address) {
        amount -= this.weiToEth(transaction.value);
      } else {
        amount += this.weiToEth(transaction.value);
      }
    });

    return amount;
  }

  public weiToEth(amount) {
    return amount / 1000000000000000000;
  }

  public toTimeStamp(datetime) {
    if (datetime.match(/^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/)) {
      const newDate = new Date(datetime);

      return newDate.getTime();
    }
    else {
      return -1;
    }
  }
}
