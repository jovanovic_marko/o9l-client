import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TokensAmountComponent } from './tokens-amount.component';

describe('TokensAmountComponent', () => {
  let component: TokensAmountComponent;
  let fixture: ComponentFixture<TokensAmountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TokensAmountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TokensAmountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
