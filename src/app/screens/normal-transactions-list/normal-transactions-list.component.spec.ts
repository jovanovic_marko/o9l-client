import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NormalTransactionsListComponent } from './normal-transactions-list.component';

describe('NormalTransactionsListComponent', () => {
  let component: NormalTransactionsListComponent;
  let fixture: ComponentFixture<NormalTransactionsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NormalTransactionsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NormalTransactionsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
