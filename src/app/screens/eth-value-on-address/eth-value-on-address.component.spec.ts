import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EthValueOnAddressComponent } from './eth-value-on-address.component';

describe('EthValueOnAddressComponent', () => {
  let component: EthValueOnAddressComponent;
  let fixture: ComponentFixture<EthValueOnAddressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EthValueOnAddressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EthValueOnAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
