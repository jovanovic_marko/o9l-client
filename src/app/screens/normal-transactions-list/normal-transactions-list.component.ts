import { Component, OnInit } from '@angular/core';
import {TransactionsService} from '../../shared/services/transactions/transactions.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-normal-transactions-list',
  templateUrl: './normal-transactions-list.component.html',
  styleUrls: ['./normal-transactions-list.component.scss']
})
export class NormalTransactionsListComponent implements OnInit {

  constructor(
    private transactionsService: TransactionsService,
    private formBuilder: FormBuilder
  ) { }

  transactionsForm: FormGroup;
  transactions = null;
  loadingTransactions = false;
  loaded = false;
  page = 1;

  ngOnInit(): void {
    this.createTransactionsForm();
  }

  public createTransactionsForm() {
    this.transactionsForm = this.formBuilder.group({
      address: ['', [Validators.required]],
      startBlock: ['', [Validators.required]]
    });
  }

  public onTransactionsFormSubmit() {
    const data = {
      address: this.transactionsForm.controls.address.value,
      start_block: this.transactionsForm.controls.startBlock.value
    };

    this.getNormalTransactions(data);
  }

  public getNormalTransactions(data) {
    this.setLoadingAndLoaded(true, false);
    this.transactionsService.getTransactions(data).subscribe(
      response => {
        console.log(response);
        this.transactions = response['result'];
        this.setLoadingAndLoaded(false, true);
      },
      error => {
        console.log(error);
        this.setLoadingAndLoaded(false, true);
      }
    );
  }

  public setLoadingAndLoaded(loading, loaded) {
    this.loaded = loaded;
    this.loadingTransactions = loading;
  }

  public weiToEth(amount) {
    return amount / 1000000000000000000;
  }
}
