import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { NormalTransactionsListComponent } from './screens/normal-transactions-list/normal-transactions-list.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { EthValueOnAddressComponent } from './screens/eth-value-on-address/eth-value-on-address.component';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import { TokensAmountComponent } from './screens/tokens-amount/tokens-amount.component';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    NormalTransactionsListComponent,
    EthValueOnAddressComponent,
    TokensAmountComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
  ],
  providers: [
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
