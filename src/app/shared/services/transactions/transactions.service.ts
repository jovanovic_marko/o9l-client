import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TransactionsService {

  constructor(
    private http: HttpClient
  ) { }

  public getTransactions(data) {
    return this.http.get(`${environment.baseUrl}/accounts/normal_transactions`, { params: data });
  }
}
