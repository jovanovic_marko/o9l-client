import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TokensService {

  constructor(
    private http: HttpClient
  ) { }

  public getTokensAmount(data) {
    return this.http.get(`${environment.baseUrl}/tokens/total_supply`, { params: data });
  }
}
