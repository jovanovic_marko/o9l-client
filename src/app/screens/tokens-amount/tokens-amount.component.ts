import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TokensService} from '../../shared/services/tokens/tokens.service';

@Component({
  selector: 'app-tokens-amount',
  templateUrl: './tokens-amount.component.html',
  styleUrls: ['./tokens-amount.component.scss']
})
export class TokensAmountComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
    private tokensService: TokensService
  ) { }

  tokensAmountForm: FormGroup;
  tokensAmount = null;
  loadingAmount = false;
  loaded = false;

  ngOnInit(): void {
    this.createTokensAmountForm();
  }

  public createTokensAmountForm() {
    this.tokensAmountForm = this.formBuilder.group({
      contractaddress: ['', [Validators.required]]
    });
  }

  public onTokensAmountFormSubmit() {
    const data = {
      contractaddress: this.tokensAmountForm.controls.contractaddress.value
    };

    this.getTokensAmount(data);
  }

  public getTokensAmount(data) {
    this.setLoadingAndLoaded(true, false);
    this.tokensService.getTokensAmount(data).subscribe(
      response => {
        console.log(response);
        this.tokensAmount = response['result'];
        this.setLoadingAndLoaded(false, true);
      },
      error => {
        console.log(error);
        this.setLoadingAndLoaded(false, true);
      }
    );
  }

  public setLoadingAndLoaded(loading, loaded) {
    this.loaded = loaded;
    this.loadingAmount = loading;
  }

}
